export default {
  conversations: [
    {
      id: 1,
      name: 'John Doe',
      sender:  'them',
      message: 'This is message by them',
      messageTime: '11:16 PM'
    },
    {
      id: 2,
      name: 'John Doe',
      sender:  'them',
      message: 'This is message by them',
      messageTime: '11:16 PM'
    },
    {
      id: 3,
      name: 'John Doe',
      sender:  'them',
      message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      messageTime: '11:17 PM'
    },
    {
      id: 4,
      name: '',
      sender:  'me',
      message: 'This is the most recent message on that chat.',
      messageTime: '11:30 PM'
    },
    {
      id: 5,
      name: '',
      sender:  'me',
      message: 'This is message by me',
      messageTime: '11:30 PM'
    },
    {
      id: 6,
      name: 'John Doe',
      sender:  'them',
      message: 'This is message by them',
      messageTime: '12:30 PM'
    },
    {
      id: 7,
      name: 'John Doe',
      sender:  'them',
      message: 'This is the most recent message on that chat.',
      messageTime: '12:43 PM'
    }
  ]
}

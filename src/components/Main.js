import React, { Component } from 'react';
import ContactList from './ContactList';
import Conversations from './Conversations';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isExpanded: false,
      selectedContact: {
        id: 1,
        name: 'John Doe',
        thumb:  '/images/user4.png',
        lastMessage: 'This is the most recent message on that chat.',
        unreadMessage: 0,
        lastMessageTime: '20 min ago',
        currentSelected: true
      }
    };
  }
  handleExpand = () => {
    this.setState({
      isExpanded: !this.state.isExpanded
    });
  }
  handleOnListClick = (contact) => {
    this.setState({
      selectedContact: contact
    })
  }
  render() {
    return (
      <div className="container">
        <div className="row main">
            <ContactList shouldExpand={this.state.isExpanded} handleOnListClick={this.handleOnListClick}/><Conversations contact={this.state.selectedContact} handleExpand={this.handleExpand}/>
        </div>
      </div>
    );
  }
}

export default Main;

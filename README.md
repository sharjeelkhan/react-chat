#React Chat

This project is created on React and styled with the help of bootstrap. The idea of this project is to create chat/messenger to be in communication with users and groups/organization.

This is simple starting prototype that includes these features:

- User should be able to search for other users and organizations
- User should be able to message any user or organization
- User should be able to message users/organizations as one of the organizations the
user belongs to
- User should be able to respond to direct messages
- User should be able to respond to messages sent to their organization
- This project is fully responsive and can be accessed on any device.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\n
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\n
You will also see any lint errors in the console.

The style is developed in SASS and you can build it with node-sass module. To build the style run this command in the project directory

### `node .\node_modules\node-sass\bin\node-sass --output-style=compressed <PROJECT PATH>\src\css\style.scss <PROJECT PATH>\src\css\style.css`


Thank you

Sharjeel Khan

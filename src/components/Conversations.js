import React, { Component } from 'react';
import conversationList from './../data/Conversations';

class Conversations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentMessage: '',
      myConversations: conversationList.conversations
    };
  }
  handleMessage = (event) => {
    this.setState({
      currentMessage: event.target.value
    });
  }
  handleSentMessage = (event) => {
    event.preventDefault();
    let conversations = this.state.myConversations;
    if(this.state.currentMessage){
      conversations.push({
        name: '',
        message: this.state.currentMessage,
        sender: 'me',
        messageTime: new Date().toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'})
      });
      this.setState({
        myConversations: conversations,
        currentMessage: ''
      }, () => {
        document.getElementsByClassName('chat-box')[0].scrollTop = document.getElementsByName('chatForm')[0].scrollHeight;
      });
    }
  }
  handleOnCLick = (event) => {
    event.preventDefault();
    this.props.handleExpand();
  }
  componentDidMount() {
    document.getElementsByClassName('chat-box')[0].scrollTop = document.getElementsByName('chatForm')[0].scrollHeight;
  }
  generateMyMessages(conversations) {
    return conversations.map((conversation, index) => {
      return (
        <div key={index} className={conversation.sender}>
          {conversation.name !== '' ? <h4>{this.props.contact.name}</h4> : ''}
          <section>
            <p>{conversation.message}</p>
            <span>{conversation.messageTime}</span>
          </section>
        </div>
      )
    });
  }
  render() {
    return (
      <div className="col col-sm-7 chat-side">
        <div className="contact-info">
          <ul>
            <li>
              <div className="user-img"><img src={this.props.contact.thumb} alt="{this.props.contact.name}"/></div>
              <div className="user-details">
                <h2>{this.props.contact.name}</h2>
                <i className="fa fa-user d-block d-sm-none" onClick={this.handleOnCLick}></i>
              </div>
            </li>
          </ul>
        </div>
        <div className="chat-box">
          <form name="chatForm">
            {this.generateMyMessages(this.state.myConversations)}
            <div className="clearfix"></div>
          </form>
        </div>
        <div className="chat-form">
          <form>
            <textarea onChange={this.handleMessage} value={this.state.currentMessage} placeholder="Type a message"></textarea>
            <button type="button" onClick={this.handleSentMessage}><i className="fa fa-paper-plane"></i></button>
          </form>
        </div>
      </div>
    );
  }
}

export default Conversations;

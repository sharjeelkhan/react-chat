import React, { Component } from 'react';
import userList from './../data/ContactList';

class ContactList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contacts: userList.contacts,
      filterList: userList.contacts
    };
  }
  filterList= (event) => {
    let filterList = this.state.contacts.filter((contact) =>
      contact.name.toLowerCase().search(event.target.value.toLowerCase()) !== -1
    );
    this.setState({filterList: filterList});
  }
  handleOnListClick = (selectedContact) => {
    this.state.contacts.map((contact, index) => {
     contact.currentSelected = false;
    });
    selectedContact.currentSelected = true;
    selectedContact.unreadMessage = 0
    this.props.handleOnListClick(selectedContact);
  }

  generateContact(contacts) {
    return contacts.map((contact, index) => {
      return (
        <li key={index} onClick={() => {this.handleOnListClick(contact)}} className={contact.currentSelected ? 'selected' : ''}>
          <div className="user-img"><img src={contact.thumb} alt={contact.name}/></div>
          <div className="user-details">
            <h2>{contact.name}</h2>
            <p>{contact.lastMessage}</p>
            {contact.unreadMessage !== 0 ? <span className="notify">{contact.unreadMessage}</span> : ''}
            <span className="time">{contact.lastMessageTime}</span>
          </div>
        </li>
      )
    });
  }
  render() {
    return (
      <div className={`col col-sm-5 contact-list ${this.props.shouldExpand ? 'expand': ''}`}>
        <div className="my-info">
          <ul>
            <li>
              <div className="user-img"><img src="images/user1.png" alt="Sharjeel Khan"/></div>
              <div className="user-details">
                <h2>Sharjeel Khan</h2>
                <i className="fa fa-cog"></i>
              </div>
            </li>
          </ul>
        </div>
        <form>
          <i className="fa fa-search"></i>
          <input type="text" placeholder="Name" onChange={this.filterList} />
        </form>
        <ul>
          {this.generateContact(this.state.filterList)}
        </ul>
      </div>
    );
  }
}

export default ContactList;

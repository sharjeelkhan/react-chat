import React, { Component } from 'react';
import Main from './components/Main';

//Importing stylesheet
import './css/style.css';

class App extends Component {
  render() {
    return (<><Main /></>);
  }
}

export default App;

export default {
  contacts: [
    {
      id: 1,
      name: 'John Doe',
      thumb:  '/images/user4.png',
      lastMessage: 'This is the most recent message on that chat.',
      unreadMessage: 0,
      lastMessageTime: '20 min ago',
      currentSelected: true
    },
    {
      id: 2,
      name: 'Sales',
      thumb:  '/images/group-list.png',
      lastMessage: 'This is the most recent message on that chat.',
      unreadMessage: 2,
      lastMessageTime: 'Yesterday',
      currentSelected: false
    },
    {
      id: 3,
      name: 'Engineering',
      thumb:  '/images/group-list.png',
      lastMessage: 'This is the most recent message on that chat.',
      unreadMessage: 0,
      lastMessageTime: '20/11/17',
      currentSelected: false
    },
    {
      id: 4,
      name: 'Garry Sobars',
      thumb:  '/images/user4.png',
      lastMessage: 'English versions from the 1914 translation by H. Rackham',
      unreadMessage: 3,
      lastMessageTime: 'Yesterday',
      currentSelected: false
    },
    {
      id: 5,
      name: 'Jeson Born',
      thumb:  '/images/users.png',
      lastMessage: 'It is a long established fact',
      unreadMessage: 0,
      lastMessageTime: 'Monday',
      currentSelected: false
    },
    {
      id: 6,
      name: 'Jimmy Jo',
      thumb:  '/images/users.png',
      lastMessage: 'All the Lorem Ipsum generators on the',
      unreadMessage: 0,
      lastMessageTime: 'Friday',
      currentSelected: false
    },
    {
      id: 7,
      name: 'John Smith',
      thumb: '/images/users.png',
      lastMessage: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested',
      unreadMessage: 0,
      lastMessageTime: 'Tuesday',
      currentSelected: false
    }
  ]
}
